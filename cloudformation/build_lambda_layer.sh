#!/bin/bash

export PKG_DIR="python"

cd lambdas/ds-layer
rm -rf ${PKG_DIR} && mkdir -p ${PKG_DIR}

docker run --rm -v $(pwd):/foo -w /foo lambci/lambda:build-python3.6 pip install -r requirements.txt -t ${PKG_DIR}

zip -r ds-layer.zip .
