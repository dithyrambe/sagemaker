import os
import boto3
import pandas as pd

# grab environment variables
BUCKET_NAME = os.environ.get("BUCKET_NAME")

runtime = boto3.client('runtime.sagemaker')
lambda_ = boto3.client("lambda")
bucket = boto3.Session().resource("s3").Bucket(BUCKET_NAME)


def load_account_transactions(account_id):
    transactions_path = f"s3://{BUCKET_NAME}/clients/transactions_{account_id}.csv"
    transactions = pd.read_csv(
        transactions_path,
        parse_dates=["date"],
        dtype={"date": "object"}
    )

    transactions["monthday"] = transactions["date"].dt.day
    transactions["date"] = transactions["date"].dt.strftime("%Y-%m-%d")
    return transactions


def process_couple(couple, transactions):
    trans1, trans2 = couple.split(",")
    transactions1 = transactions[transactions["id"] == int(trans1)][["amount", "date", "label", "longLabel"]].iloc[0].to_dict()
    transactions2 = transactions[transactions["id"] == int(trans2)][["amount", "date", "label", "longLabel"]].iloc[0].to_dict()
    return {
        couple.replace(',', '-'): {
            trans1: transactions1,
            trans2: transactions2
        }
    }


def load_account_to_labelize(account_id):
    al_db_path = f"s3://{BUCKET_NAME}/active_learning/db_al.csv"
    to_labelize = pd.read_csv(al_db_path)
    to_labelize = to_labelize[to_labelize.iloc[:, -1] == account_id]
    to_labelize = to_labelize[to_labelize.iloc[:, -2].isnull()]
    account_trans = load_account_transactions(account_id)
    result = {"pairs": [process_couple(couple, account_trans) for couple in to_labelize.values[:, 0]]}
    return result


def lambda_handler(event, context):
    account_id = event["account_id"]
    return load_account_to_labelize(account_id)
