import os
import boto3
import json
import pandas as pd

# grab environment variables
CLUSTERING_ENDPOINT = os.environ.get("CLUSTERING_ENDPOINT")
BUCKET_NAME = os.environ.get("BUCKET_NAME")
LAMBDA_AL_NAME = os.environ.get("LAMBDA_AL_NAME")

runtime = boto3.client('runtime.sagemaker')
lambda_ = boto3.client("lambda")


def load_account_transactions(account_id):
    transactions_path = f"s3://{BUCKET_NAME}/clients/transactions_{account_id}.csv"
    transactions = pd.read_csv(
        transactions_path,
        parse_dates=["date"],
        dtype={"date": "object"}
    )

    transactions["monthday"] = transactions["date"].dt.day
    return transactions


def lambda_handler(event, context):
    account_id = event["account_id"]
    user_trans = load_account_transactions(account_id)

    payload = user_trans.to_csv(index=False)
    response = runtime.invoke_endpoint(
        EndpointName=CLUSTERING_ENDPOINT,
        ContentType='text/csv',
        Body=payload
    )

    body = json.loads(response["Body"].read().decode())
    al_record = body["results"]["data_to_labelize"]
    al_record["0"]["16"] = account_id

    al_payload = json.dumps(al_record).replace("NaN", "null")
    lambda_.invoke(
        FunctionName=LAMBDA_AL_NAME,
        InvocationType='Event',
        Payload=al_payload
    )

    return {
        "clusters": body["results"]["clusters"]
    }
