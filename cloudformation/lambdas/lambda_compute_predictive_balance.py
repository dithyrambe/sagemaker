import boto3
import json
import time
import pandas as pd
from datetime import datetime

from boto3.dynamodb.conditions import Attr

BUCKET_NAME = "patoche"
PREFIX_CONTRACT = "CSV/CR812/contract_element_812.csv"
PREFIX_PERIMETER = "CSV/CR812/perimeter_812.csv"
PREFIX_BALANCE = "CSV/CR812/balance_history_812.csv"
PREFIX_OPERATION = "CSV/CR812/operation_812.csv"

dynamodb = boto3.resource('dynamodb')
table_ops = dynamodb.Table("operation_predictive_3")
table_balance = dynamodb.Table("predictive_balance")
s3 = boto3.resource('s3')
s3_cli = boto3.client('s3')
sns = boto3.client('sns')

obj_contracts = s3_cli.get_object(Bucket=BUCKET_NAME, Key=PREFIX_CONTRACT)
df_contracts = pd.read_csv(obj_contracts['Body'])
obj_perimeters = s3_cli.get_object(Bucket=BUCKET_NAME, Key=PREFIX_PERIMETER)
df_perimeters = pd.read_csv(obj_perimeters['Body'])
obj_balances = s3_cli.get_object(Bucket=BUCKET_NAME, Key=PREFIX_BALANCE)
df_balances = pd.read_csv(obj_balances['Body'])
obj_operations = s3_cli.get_object(Bucket=BUCKET_NAME, Key=PREFIX_OPERATION)
dtypes = {
    'date': 'object'
}
df_operations = pd.read_csv(obj_operations['Body'], dtype=dtypes)
df_operations['date'] = pd.to_datetime(df_operations['date'], format="%Y-%m-%d")

BALANCE_DATE = "2019-01-31"
BALANCE_VALIDATION_DATE = "2019-02-28"
STARTING_DATE = "2019-02-15"


def lambda_handler(event, context):
    client_id = event['client_id']

    client_operations = df_operations[df_operations["contractElementId"] == client_id]
    operations_to_add = client_operations[(client_operations["date"] > BALANCE_DATE)
                                          & (client_operations["date"] < STARTING_DATE)]
    sum_real_ops = operations_to_add["amount"].sum()

    predictive_ops = table_ops.scan(
        FilterExpression=Attr('client_id').eq(int(client_id))
    )['Items']
    sum_predictive_ops = sum([int(x['montant']) for x in predictive_ops
                              if x['date'] > datetime.strptime(STARTING_DATE, "%Y-%m-%d").day])

    balance_previous_month = df_balances[(df_balances["balanceDate"] == BALANCE_DATE)
                                         & (df_balances["contractElementId"] == client_id)]["balance"].values[0]

    predictive_balance = balance_previous_month + sum_real_ops + sum_predictive_ops

    filter_contract = df_contracts['id'] == client_id
    try:
        cr_id = df_contracts[filter_contract]['crId'].iloc[0]
    except Exception:
        print("problème recupération de CrID for contractElementId {}".format(client_id))
        return "Quiqui"
    contract_element_number = df_contracts[filter_contract]['contractElementNumber'].iloc[0]
    partner_id = df_contracts[filter_contract]['partnerId'].iloc[0]
    filter_perimeter = df_perimeters['perimeterId'] == partner_id
    try:
        cael_id = df_perimeters[filter_perimeter]['caelId'].iloc[0]
    except Exception:
        print("problème recupération de caelId for contractElementId {}".format(client_id))
        return "Quiqui"

    params = {
        "clientId": str(cael_id),
        "structureId": str(cr_id),
        "accountNumber": str(contract_element_number),
        "dateCalculation": int(time.mktime(datetime.strptime(str(BALANCE_VALIDATION_DATE), "%Y-%m-%d").timetuple())),
        "amount": str(predictive_balance)
    }

    sns.publish(
        TopicArn="arn:aws:sns:eu-west-1:865488915276:capture-master-SharedResources-SoldePrevisionnelSNSTopic-DL45MF3QDI81",
        Message=json.dumps(params)
    )
