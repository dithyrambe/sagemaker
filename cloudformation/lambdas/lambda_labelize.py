import os
import io
import boto3
import pandas as pd

# grab environment variables
BUCKET_NAME = os.environ.get("BUCKET_NAME")

runtime = boto3.client('runtime.sagemaker')
lambda_ = boto3.client("lambda")
bucket = boto3.Session().resource("s3").Bucket(BUCKET_NAME)


def labelize_record(account_id, couple, label):
    al_db_path = f"s3://{BUCKET_NAME}/active_learning/db_al.csv"
    df = pd.read_csv(al_db_path)
    df.iloc[:, -2][(df.iloc[:, -1] == account_id) & (df.iloc[:, 0] == couple)] = label
    buffer = io.StringIO()
    df.to_csv(buffer, index=False)
    response = bucket.Object("active_learning/db_al.csv").put(Body=buffer.getvalue())
    return response


def lambda_handler(event, context):
    account_id = event["account_id"]
    couple = event["couple"]
    label = event["label"]
    response = labelize_record(account_id, couple, label)
    return {"response": response}
