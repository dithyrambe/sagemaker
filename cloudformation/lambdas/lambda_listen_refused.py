import boto3
import json
import os

LAMBDA_PREDICTIVE_BALANCE = os.environ['LAMBDA_PREDICTIVE_BALANCE']


def lambda_handler(event, context):
    print("EVENT --> ", event)
    sns_message = json.loads(event["Records"][0]["Sns"]["Message"])
    lambda_ = boto3.client("lambda")
    operation_id = sns_message["operationId"]
    print("SNS MESSAGE --> ", sns_message)
    # if sns_message.get("status")=="REFUSED":
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('operation_predictive_3')
    table.update_item(
        Key={
            "operation_id": operation_id
        },
        UpdateExpression="set statut = :r",
        ExpressionAttributeValues={
            ":r": sns_message.get("status")
        },
        ReturnValues="UPDATED_NEW"
    )

    payload = {
        "client_id": operation_id.split("_")[0]
    }
    lambda_.invoke(
        FunctionName=LAMBDA_PREDICTIVE_BALANCE,
        InvocationType="Event",
        Payload=json.dumps(payload)
    )
