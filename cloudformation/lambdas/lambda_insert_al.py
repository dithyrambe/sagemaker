import os
import io
import boto3
import pandas as pd

# grab environment variables
BUCKET_NAME = os.environ.get("BUCKET_NAME")

runtime = boto3.client('runtime.sagemaker')
lambda_ = boto3.client("lambda")
bucket = boto3.Session().resource("s3").Bucket(BUCKET_NAME)


def insert_unlabeled_record(record):
    al_db_path = f"s3://{BUCKET_NAME}/active_learning/db_al.csv"
    df = pd.read_csv(al_db_path)
    record_df = pd.DataFrame.from_dict(record, orient="index")
    df = df.append(record_df)
    buffer = io.StringIO()
    df.to_csv(buffer, index=False)
    response = bucket.Object("active_learning/db_al.csv").put(Body=buffer.getvalue())
    return response


def lambda_handler(event, context):
    response = insert_unlabeled_record(event)
    return {"response": response}
