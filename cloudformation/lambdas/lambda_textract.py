import pandas as pd
import json
import boto3
import uuid
from datetime import datetime


def get_rows_columns_map(table_result, blocks_map):

    rows = {}
    for relationship in table_result['Relationships']:
        if relationship['Type'] == 'CHILD':
            for child_id in relationship['Ids']:
                cell = blocks_map[child_id]
                if cell['BlockType'] == 'CELL':
                    row_index = cell['RowIndex']
                    col_index = cell['ColumnIndex']
                    if row_index not in rows:
                        rows[row_index] = {}
                    rows[row_index][col_index] = get_text(cell, blocks_map)
    return rows


def get_text(result, blocks_map):

    text = ''
    if 'Relationships' in result:
        for relationship in result['Relationships']:
            if relationship['Type'] == 'CHILD':
                for child_id in relationship['Ids']:
                    word = blocks_map[child_id]
                    if word['BlockType'] == 'WORD':
                        text += word['Text'] + ' '
    return text


def _try_datetime_cast(series):

    is_datetime = False
    try:
        pd.to_datetime(series)
        is_datetime = True
    except (ValueError, TypeError) as err:
        pass
    return is_datetime


def try_datetime_cast(df):
    return any([_try_datetime_cast(df[x]) for x in df.columns])


def get_tables_json_results(data):

    response = data
    blocks = response['Blocks']
    blocks_map = {}
    table_blocks = []
    for block in blocks:
        blocks_map[block['Id']] = block
        if block['BlockType'] == "TABLE":
            table_blocks.append(block)
    json = {"tables": []}
    for index, table in enumerate(table_blocks):
        json_table = generate_table_json(table, blocks_map, index + 1)
        if json_table:
            json["tables"].append(json_table)

    return json


def generate_table_json(table_result, blocks_map, table_index):
    rows = get_rows_columns_map(table_result, blocks_map)
    raw_rows = [list([y.strip() for y in x.values()]) for x in rows.values()]
    df = pd.DataFrame(raw_rows[1:], columns=raw_rows[0])
    json_table = {}
    if try_datetime_cast(df):
        json_table = df.to_dict(orient='records')
    return json_table


def _push_predictive_op_in_dynamo(table, **kwargs):
    date = datetime.now().isoformat()
    unique_id = str(kwargs.get('client_id')) + '_' + str(uuid.uuid4())
    table.put_item(
        Item={
            'client_id': kwargs.get('client_id'),
            'last_update_time': date,
            'operation_id': unique_id,
            'date': kwargs.get('date'),
            'operation_type': "Facture",
            'creation_time': date,
            'montant': kwargs.get('amount'),
            'label': "Echéance",
            'nature': 'Debit',
            'structure_id': str(kwargs.get('cr_id')),
            'caelId': kwargs.get('cael_id'),
            'statut': 'VALIDE',
            'account_number': str(kwargs.get('contract_element_number'))
        }
    )
    print("Predictive operation {} pushed".format(unique_id))


def lambda_handler(event, context):
    print("EVENT --> ", event)
    sns_message = json.loads(event["Records"][0]["Sns"]["Message"])
    print("SNS MESSAGE --> ", sns_message)
    in_bucket = sns_message["s3Bucket"]
    in_key = sns_message["objectKey"]
    cael_id = sns_message["clientId"]

    s3 = boto3.client('s3')
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('operation_predictive')

    BUCKET_NAME = "patoche"
    PREFIX_CONTRACT = "CSV/CR812/contract_element_812.csv"
    PREFIX_PERIMETER = "CSV/CR812/perimeter_812.csv"

    obj_contracts = s3.get_object(Bucket=BUCKET_NAME, Key=PREFIX_CONTRACT)
    df_contracts = pd.read_csv(obj_contracts['Body'])
    obj_perimeters = s3.get_object(Bucket=BUCKET_NAME, Key=PREFIX_PERIMETER)
    df_perimeters = pd.read_csv(obj_perimeters['Body'])

    df_perimeters = df_perimeters[df_perimeters["caelId"] == int(cael_id)]
    perimeter_id = df_perimeters["perimeterId"].values[0]
    contract_element_id, contract_element_number, cr_id = df_contracts[df_contracts["partnerId"] == perimeter_id][
                                                                    ["id", "contractElementNumber", "crId"]].iloc[0]

    response = s3.get_object(Bucket=in_bucket, Key=in_key)
    data = json.loads(response["Body"].read().decode("utf8"))
    json_results = get_tables_json_results(data)

    predictive_op = json_results["tables"][0][0]
    params = {
        "client_id": contract_element_id,
        "date": datetime.strptime(predictive_op["Date"], "%d/%m/%Y").day,
        "amount": predictive_op["Echeance"],
        "cr_id": cr_id,
        "cael_id": cael_id,
        "contract_element_number": contract_element_number
    }
    _push_predictive_op_in_dynamo(table, **params)

    return json_results
