import boto3
import json
import time

from boto3.dynamodb.conditions import Key
from datetime import datetime

CAEL_ID = 38577738992
DYNAMO_PRED_BALANCE_TABLE = "predictive_balance"
SNS_ARN_TOPIC = "arn:aws:sns:eu-west-1:865488915276:capture-master-SharedResources-SoldePrevisionnelSNSTopic-DL45MF3QDI81"

dynamodb = boto3.resource('dynamodb')
sns = boto3.client('sns')

table = dynamodb.Table(DYNAMO_PRED_BALANCE_TABLE)
response = table.query(
    IndexName="client_id-index",
    KeyConditionExpression=Key('client_id').eq(CAEL_ID)
)
predictive_balance = response['Items'][0]
params = {
    "clientId": str(predictive_balance['client_id']),
    "structureId": str(predictive_balance['structure_id']),
    "accountNumber": str(predictive_balance['account_number']),
    "dateCalculation": int(time.mktime(datetime.strptime(predictive_balance['date'], "%Y-%m-%d").timetuple())),
    "amount": str(predictive_balance['balance'])
}
sns.publish(
    TopicArn=SNS_ARN_TOPIC,
    Message=json.dumps(params)
)
