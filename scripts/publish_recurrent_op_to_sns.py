import boto3
import json
import time

from datetime import datetime

SNS_ARN_TOPIC = "arn:aws:sns:eu-west-1:865488915276:operation_predictive"

dynamodb = boto3.resource('dynamodb')
sns = boto3.client('sns')

recurrent_op = {
    "nature": "Credit",
    "statut": "VALIDE",
    "operation_type": "Historique",
    "client_id": "60000229499",
    "creation_time": "1556755200",
    "label": "eFD 82 C0q5 CLstkKQ DU 12/6391",
    "montant": "1768",
    "account_number": "65893559663",
    "last_update_time": "1556755200",
    "date": "6",
    "structure_id": "812",
    "operation_id": "60000229499_b3ae93df-c66c-4d2d-a97a-49872b361631",
    "caelId": "879664"
}


def publish_op(**params):
    params['client_id'] = str(params['client_id'])
    params['montant'] = str(params['montant'])
    params['date'] = str(params['date'])
    params['creation_time'] = str(
        int(time.mktime(datetime.strptime(params['creation_time'].split("T")[0], "%Y-%m-%d").timetuple())))
    params['last_update_time'] = str(
        int(time.mktime(datetime.strptime(params['last_update_time'].split("T")[0], "%Y-%m-%d").timetuple())))
    sns.publish(
        TopicArn=SNS_ARN_TOPIC,
        Message=json.dumps(params)
    )


publish_op(**recurrent_op)
