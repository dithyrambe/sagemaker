import boto3
client = boto3.client('sagemaker')
response = client.describe_endpoint(
    EndpointName='sagemaker-active-learning-2019-04-18-07-12-35-437'
)
model_data = client.describe_training_job(TrainingJobName="sagemaker-active-learning-2019-04-18-07-12-35-437")
model_data = model_data["ModelArtifacts"]["S3ModelArtifacts"]
image = '865488915276.dkr.ecr.eu-west-1.amazonaws.com/sagemaker-active-learning:latest'
primary_container = {
    "Image": image,
    "ModelDataUrl": model_data
}
role = 'arn:aws:iam::865488915276:role/service-role/AmazonSageMaker-ExecutionRole-20190326T223875'
create_model_response = client.create_model(
    ModelName="coucou",
    ExecutionRoleArn=role,
    PrimaryContainer=primary_container)
endpoint_config = client.describe_endpoint_config(
    EndpointConfigName='sagemaker-active-learning-2019-04-18-07-12-35-437'
)
endpoint_config['ProductionVariants'].append({
    "InstanceType": "ml.c4.2xlarge",
    "InitialInstanceCount": 1,
    "ModelName": "coucou",
    "VariantName": "Coucou"
})
new_endpoint_config = client.create_endpoint_config(
    EndpointConfigName="sagemaker-new-conf-with-coucou",
    ProductionVariants=endpoint_config['ProductionVariants']
)
response = client.update_endpoint(
    EndpointName="sagemaker-active-learning-2019-04-18-07-12-35-437",
    EndpointConfigName="sagemaker-new-conf-with-coucou"
)
print(response)
