import pandas as pd
import numpy as np
import boto3
import io

CR_ID = "868"

DATE_COLS = ("date", "deferredDebitDate", "balanceDate")
NAMES_LOWER_CPTE_CHQ = ("cchq", "compte courant")
LABELS_TO_DROP = ["PAIEMENT PAR CARTE", "RETRAIT AU DISTRIBUTEUR"]

OUT_BUCKET = "labs-sagemaker-datalab-ca"
OUT_KEY_REC_OP = "out_recurrent_op-0205/{}".format(CR_ID)
BUCKET_NAME_CONTRACT = "patoche"
PREFIX_CONTRACT = "CSV/CR{}/contract_element_{}.csv".format(CR_ID, CR_ID)
BUCKET_NAME_OPS = "patoche"
PREFIX_OPS = "CSV/CR{}/operation_{}.csv".format(CR_ID, CR_ID)

s3_cli = boto3.client('s3')


def filter_cpte_chq(op, contracts, libs=NAMES_LOWER_CPTE_CHQ):
    contract_chq = contracts["name"].str.lower().str.contains("|".join(map(str.lower, libs)))
    filtered_op = op[
        op["contractElementId"].isin(
            contracts[contract_chq]["id"].unique()
        )
    ]
    return filtered_op


def remove_labeled_operations(df, labels_to_drop=LABELS_TO_DROP):
    return df[~df["longLabel"].isin(LABELS_TO_DROP)]


def add_noise(series):
    noise = np.random.random(len(series)) - 0.5
    return series + noise


def date_to_closed_polar_day(series, noise=False):
    days = series.dt.day
    if noise:
        days = add_noise(days)
    res = days / (series.dt.daysinmonth - 1)
    return res * 2 * np.pi


obj_contract = s3_cli.get_object(Bucket=BUCKET_NAME_CONTRACT, Key=PREFIX_CONTRACT)
df_contracts = pd.read_csv(obj_contract['Body'])

obj_operation = s3_cli.get_object(Bucket=BUCKET_NAME_OPS, Key=PREFIX_OPS)
dtypes = {
    'date': 'object'
}
df_operations = pd.read_csv(obj_operation['Body'], dtype=dtypes)
df_operations = df_operations[~(df_operations["date"] == "0000-00-00")]
df_operations = df_operations[(df_operations["date"] > "2018-07-01") & (df_operations["date"] < "2019-01-31")]
print(df_operations.sort_values("date")["date"].value_counts())
df_operations['date'] = pd.to_datetime(df_operations['date'], format="%Y-%m-%d")

op_chq = filter_cpte_chq(df_operations, df_contracts)[
    ["id", "contractElementId", "date", "deferredDebitDate", "label", "longLabel", "amount"]]
op_chq = remove_labeled_operations(op_chq, labels_to_drop=LABELS_TO_DROP)

op_chq["s_amount"] = op_chq["amount"]
op_chq["monthday"] = op_chq["date"].dt.day
op_chq["closed_polar_day"] = date_to_closed_polar_day(op_chq["date"], noise=True)

s3_bucket = boto3.Session().resource('s3').Bucket(OUT_BUCKET)


def push_clients_operations(client_id, operations):
    sub = operations[operations["contractElementId"] == client_id]
    csv_buffer = io.StringIO()
    sub.to_csv(csv_buffer, index=False)
    s3_object = s3_bucket.Object("{}/transactions_{}.csv".format(OUT_KEY_REC_OP, client_id))
    s3_object.put(Body=csv_buffer.getvalue())


for client_id in op_chq["contractElementId"].unique():
    push_clients_operations(client_id, op_chq)
