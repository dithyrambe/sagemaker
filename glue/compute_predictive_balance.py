import os
import boto3
import uuid
import pandas as pd
from datetime import datetime

from boto3.dynamodb.conditions import Attr

CR_ID = "812"

DYNAMO_REC_OPS_TABLE = os.environ['DYNAMO_REC_OPS_TABLE']
DYNAMO_PRED_BALANCE_TABLE = os.environ['DYNAMO_REC_OPS_TABLE']

BUCKET_NAME = "patoche"
PREFIX_CONTRACT = "CSV/CR{}/contract_element_{}.csv".format(CR_ID, CR_ID)
PREFIX_PERIMETER = "CSV/CR{}/perimeter_{}.csv".format(CR_ID, CR_ID)
PREFIX_BALANCE = "CSV/CR{}/balance_history_{}.csv".format(CR_ID, CR_ID)
PREFIX_OPERATION = "CSV/CR{}/operation_{}.csv".format(CR_ID, CR_ID)

BALANCE_DATE = "2019-01-31"
BALANCE_VALIDATION_DATE = "2019-02-28"
STARTING_DATE = "2019-02-15"

dynamodb = boto3.resource('dynamodb')
table_ops = dynamodb.Table(DYNAMO_REC_OPS_TABLE)
table_balance = dynamodb.Table(DYNAMO_PRED_BALANCE_TABLE)
s3 = boto3.resource('s3')
s3_cli = boto3.client('s3')

obj_contracts = s3_cli.get_object(Bucket=BUCKET_NAME, Key=PREFIX_CONTRACT)
df_contracts = pd.read_csv(obj_contracts['Body'])
obj_perimeters = s3_cli.get_object(Bucket=BUCKET_NAME, Key=PREFIX_PERIMETER)
df_perimeters = pd.read_csv(obj_perimeters['Body'])
obj_balances = s3_cli.get_object(Bucket=BUCKET_NAME, Key=PREFIX_BALANCE)
df_balances = pd.read_csv(obj_balances['Body'])
obj_operations = s3_cli.get_object(Bucket=BUCKET_NAME, Key=PREFIX_OPERATION)
dtypes = {
    'date': 'object'
}
df_operations = pd.read_csv(obj_operations['Body'], dtype=dtypes)
df_operations['date'] = pd.to_datetime(df_operations['date'], format="%Y-%m-%d")

client_ids = df_contracts["id"].unique()


# THIS PART IS ONLY NEEDED BECAUSE DATA TABLES ARE NOT CONSISTENT (SHOULD BE REMOVED WITH REAL DATA)
def filter_consistent_data(balances, ids, balance_date, balance_validation_date):
    ids_before = balances[(balances["contractElementId"].isin(ids))
                          & (balances["balanceDate"] == balance_date)]["contractElementId"].unique()
    ids_after = balances[(balances["contractElementId"].isin(ids))
                         & (balances["balanceDate"] == balance_validation_date)]["contractElementId"].unique()
    return list(set.intersection(set(ids_before), set(ids_after)))


client_ids = filter_consistent_data(df_balances, client_ids, BALANCE_DATE, BALANCE_VALIDATION_DATE)
print("Working on {} clients".format(len(client_ids)))
for client_id in client_ids:
    client_operations = df_operations[df_operations["contractElementId"] == client_id]
    operations_to_add = client_operations[(client_operations["date"] > BALANCE_DATE)
                                          & (client_operations["date"] < STARTING_DATE)]
    sum_real_ops = operations_to_add["amount"].sum()

    predictive_ops = table_ops.scan(
        FilterExpression=Attr('client_id').eq(int(client_id))
    )['Items']
    sum_predictive_ops = sum([int(x['montant']) for x in predictive_ops
                              if x['date'] > datetime.strptime(STARTING_DATE, "%Y-%m-%d").day])

    balance_previous_month = df_balances[(df_balances["balanceDate"] == BALANCE_DATE)
                                         & (df_balances["contractElementId"] == client_id)]["balance"].values[0]
    balance_next_month = df_balances[(df_balances["balanceDate"] == BALANCE_VALIDATION_DATE)
                                     & (df_balances["contractElementId"] == client_id)]["balance"].values[0]

    predictive_balance = balance_previous_month + sum_real_ops + sum_predictive_ops

    filter_contract = df_contracts['id'] == client_id
    try:
        cr_id = df_contracts[filter_contract]['crId'].iloc[0]
    except Exception:
        print("problème recupération de CrID for contractElementId {}".format(client_id))
        continue
    contract_element_number = df_contracts[filter_contract]['contractElementNumber'].iloc[0]
    partner_id = df_contracts[filter_contract]['partnerId'].iloc[0]
    filter_perimeter = df_perimeters['perimeterId'] == partner_id
    try:
        cael_id = df_perimeters[filter_perimeter]['caelId'].iloc[0]
    except Exception:
        print("problème recupération de caelId for contractElementId {}".format(client_id))
        continue

    table_balance.put_item(
        Item={
            'predictive_balance_id': str(uuid.uuid4()),
            'client_id': int(cael_id),
            'contract_element_id': int(client_id),
            'creation_time': str(datetime.now()),
            'date': str(BALANCE_VALIDATION_DATE),
            'balance': int(predictive_balance),
            'structure_id': int(cr_id),
            'account_number': int(contract_element_number)
        }
    )
