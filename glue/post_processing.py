import boto3
import json
import uuid
import pandas as pd
from datetime import datetime

CR_ID = "868"

CLUSTERING_ENDPOINT = "sagemaker-active-learning-2019-04-25-14-05-02-682"

BUCKET_NAME_CUS = "labs-sagemaker-datalab-ca"
PREFIX_CUS = "out_recurrent_op-0205/{}/transactions_".format(CR_ID)

BUCKET_NAME = "patoche"
PREFIX_CONTRACT = "CSV/CR{}/contract_element_{}.csv".format(CR_ID, CR_ID)
PREFIX_PERIMETER = "CSV/CR{}/perimeter_{}.csv".format(CR_ID, CR_ID)
PREFIX_BALANCE = "CSV/CR{}/balance_history_{}.csv".format(CR_ID, CR_ID)

BALANCE_DATE = "2019-01-31"
BALANCE_VALIDATION_DATE = "2019-02-28"

runtime = boto3.client('runtime.sagemaker')
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('operation_predictive_3')
s3 = boto3.resource('s3')
s3_cli = boto3.client('s3')

obj_contract = s3_cli.get_object(Bucket=BUCKET_NAME, Key=PREFIX_CONTRACT)
df_contracts = pd.read_csv(obj_contract['Body'])

obj_perimeter = s3_cli.get_object(Bucket=BUCKET_NAME, Key=PREFIX_PERIMETER)
df_perimeter = pd.read_csv(obj_perimeter['Body'])

obj_balance = s3_cli.get_object(Bucket=BUCKET_NAME, Key=PREFIX_BALANCE)
df_balance = pd.read_csv(obj_balance['Body'])

client_ids = df_contracts["id"].unique()


# THIS PART IS ONLY NEEDED BECAUSE DATA TABLES ARE NOT CONSISTENT (SHOULD BE REMOVED WITH REAL DATA)
def filter_consistent_data(balances, ids, balance_date, balance_validation_date):
    ids_before = balances[(balances["contractElementId"].isin(ids))
                          & (balances["balanceDate"] == balance_date)]["contractElementId"].unique()
    ids_after = balances[(balances["contractElementId"].isin(ids))
                         & (balances["balanceDate"] == balance_validation_date)]["contractElementId"].unique()
    return list(set.intersection(set(ids_before), set(ids_after)))


client_ids = filter_consistent_data(df_balance, client_ids, BALANCE_DATE, BALANCE_VALIDATION_DATE)
print("Working on {} clients".format(len(client_ids)))
now = datetime.now().isoformat()
bucket_transactions = s3.Bucket(BUCKET_NAME_CUS)
for obj in bucket_transactions.objects.filter(Prefix=PREFIX_CUS):
    contract_element_id = int(obj.key.split("/")[2].split("_")[1].split(".")[0])

    if contract_element_id in client_ids:

        filter_contract = df_contracts['id'] == contract_element_id
        try:
            cr_id = df_contracts[filter_contract]['crId'].iloc[0]
        except Exception:
            print("problème recupération de CrID for contractElementId {}".format(contract_element_id))
            continue
        contractElmentNumber = df_contracts[filter_contract]['contractElementNumber'].iloc[0]
        partnerId = df_contracts[filter_contract]['partnerId'].iloc[0]
        filter_perimeter = df_perimeter['perimeterId'] == partnerId
        try:
            cael_id = df_perimeter[filter_perimeter]['caelId'].iloc[0]
        except Exception:
            print("problème recupération de caelId for contractElementId {}".format(contract_element_id))
            continue
        transactions_path = 's3://{}/{}'.format(BUCKET_NAME_CUS, PREFIX_CUS + str(contract_element_id) + ".csv")
        dtypes = {
            'date': 'object'
        }
        transactions = pd.read_csv(transactions_path, parse_dates=["date"], dtype=dtypes)
        transactions["monthday"] = transactions["date"].dt.day

        payload = transactions.to_csv(index=False)
        try:
            response = runtime.invoke_endpoint(
                EndpointName=CLUSTERING_ENDPOINT,
                ContentType='text/csv',
                Body=payload,
            )
            data = response['Body'].read().decode()
            clusters = json.loads(data)['results']['clusters']

            for key, value in clusters.items():
                label = value["label"]
                montant = round(value["s_amount"])
                nature = "Credit" if montant > 0 else "Debit"
                date = round(value["monthday"])
                table.put_item(
                    Item={
                        'client_id': contract_element_id,
                        'last_update_time': now,
                        'operation_id': str(contract_element_id) + '_' + str(uuid.uuid4()),
                        'date': date,
                        'label': label,
                        'montant': montant,
                        'nature': nature,
                        'structure_id': str(cr_id),
                        'account_number': str(contractElmentNumber),
                        'caelId': str(cael_id),
                        'statut': "VALIDE",
                        'creation_time': now,
                        'operation_type': "Historique"
                    }
                )
        except (Exception, runtime.exceptions.ModelError) as err:
            print("No clusters for client id - ne dispose pas d'assez de données {}".format(str(contract_element_id)))
