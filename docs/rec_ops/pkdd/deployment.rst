Déploiement
===========

Déploiement Sagemaker
---------------------

Une fois l'algorithme de clustering finalisé, il est déployé en tant qu'endpoint Sagemaker. Ce endpoint accepte en entrée des 
CSV sérialisés et retourne des CSV sérialisés.

Déploiement API Gateway & Lambda
--------------------------------

Afin de pouvoir réaliser une démo ainsi que des tests, un route API Gateway a été mise en place.
Cette API déclenche une Lambda qui récupère les transactions correspondant à l'`account_id` passé en paramètre. Ensuite,
le endpoint Sagemaker est appelé pour réaliser le clustering sur ces transactions et obtenir les opérations récurrentes.


Voici un exemple d'appel API pour le client vu précedemment:

.. image:: img/rec_ops_postman.png
