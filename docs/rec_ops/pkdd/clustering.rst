Clustering
==========

Mode opératoire
---------------

Afin de détecter les opérations récurrentes, nous allons utiliser un algorithme de clustering afin de regrouper toutes les opérations
récurrentes d'un client en clusters. Par exemple, le but est de regrouper toutes les transactions d'un client en un cluster.

Algorithme
----------

L'algorithme de clustering utilisé pour faire ce clustering est l'algorithme **DBSCAN**, plus particulièrement son implémentation 
dans `scikit-learn <https://scikit-learn.org/stable/modules/generated/sklearn.cluster.DBSCAN.html>`_.

Exemple
-------

Voici un exemple de résultat pour un individu de la base OpenData utilisée:

.. image:: img/rec_ops_data_viz_clustering.png

Les différents points de même couleur représentent un cluster et donc une opération récurrente.
On peut par exemple supposer que les opérations bleues correspondent au salaire du client les violettes à son loyer.
