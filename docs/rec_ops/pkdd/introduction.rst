Introduction
============
Le but de cette partie est de pouvoir détecter les opérations récurrentes sur les comptes chèques des clients.

Données à disposition
---------------------
Dans un premier temps, les données MaBanque n'étant pas disponibles, nous utilisons une base de données OpenData similaires aux données MaBanque.
On peut retrouver un déscriptif complet de cette base de données `ici <https://sorry.vse.cz/~berka/challenge/PAST/index.html>`_.

Voici un exemple de la table `transaction` de cette base de données qui nous sert à simuler la table `operations` des données MaBanque:

.. image:: img/rec_ops_pkdd_data.png

Les seules informations utilisées, dans un premier temps, pour la détection des opérations récurrentes sont les colonnes:
 - `date`: la date a laquelle la transaction a été effectuée
 - `amount` et `type`: permettant de construire le montant de la transaction
