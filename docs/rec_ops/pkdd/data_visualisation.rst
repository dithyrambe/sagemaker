Visualisation des données
=========================

La première étape est de préparer les données afin de pouvoir les visualiser et de préparer la phase de clustering.
On représente alors les données comme ci-dessous:

.. image:: img/rec_ops_data_viz.png