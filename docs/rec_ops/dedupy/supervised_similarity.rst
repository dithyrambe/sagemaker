Approche supervisée
-------------------
Lors de cette phase, chaque couple d'opération présent dans l'historique d'un client est présenté à un algorithme de classification. La tâche du modèle est de spécifier si le couple présenté est constitué de deux opérations identiques ne différant que par la date à laquelle la trasaction est effectuée (dans ce les deux opérations appartiennent au même cluster et constituent en partie une opération récurrentes. Par exmple deux salaires), ou si ces deux opérations sont distinctes (elles n'appartiendrons pas au même cluster. Par exemple une facture et un loyer).

L'espace de représentation de l'algorithme est un espace de distance. Chaque distance représente une dimension dans laquelle l'algorithme appendra sa séparatrice.

Distances utilisées:
 - ``LongestCommonSubstring``: taille de la chaine de caractère commune la plus longue entre les libellés du couple d'opération
 - ``LevensteinDistance``: distance de Levenstein entre les libellés du couple d'opération
 - ``AmountDistance``: carré de la différences des deux montants
 - ``ClosedPolarDist``: distance autour du cercles des jours dans le mois (1er du mois et 31 du mois à 0 et 15 du mois à pi)

Afin de construire un matrice de similarité, c'est la probabilité prédite par l'algorithme qui alimentera la matrice de similarité.
Si un RandomForest à été choisi pour le POC, la nature de l'algorithme de classification est peu déterminante.
