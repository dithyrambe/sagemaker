Introduction
============
Le jeu de donnée ouvert PKDD99 semble être un jeu de donnée synthétique. Les opérations récurrentes partagent exactement les mêmes caractéristiques. Lorsque le modèle de clustering est appliqué aux données réelles des clients MaBanque, les performances sont fortement dégradées. Une nouvelle approche a dû être mise en place pour identifier les clusters d'opérations récurrentes.

Cette méthode se décompose en deux étapes: 
 - la construction d'une matrice de similarité s'appuyant sur un algorithme de classification supervisé
 - l'identification des éléments connectés après seuillage de cette matrice.
