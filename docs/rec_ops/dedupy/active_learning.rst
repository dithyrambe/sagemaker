
Active learning
===============
Afin d'avoir une matrice de similarité pertinente, il a été nécessaire d'entrainer le modèle à reconnaitre des transactions similaires espacées dans le temps.
La constitution du dataset d'apprentissage à été fait à l'aide de l'implémentation d'un module d'active learning.


Initialisation
--------------
Dans un premier temps, des couples d'opération d'un même client sont tirés aléatoirement et poussés à un opérateur afin que le label soit manuellement saisi. Le label est binaire (oui/non).
Ces premiers couples et leur label constituent une base d'apprentissage pour un algorithme d'apprentissage supervisé.o


Active learning
---------------
Afin d'enrichir le jeu d'apprentissage, de nouvelles paires d'opération sont poussées à un opérateur afin d'être labélisées. Cette fois les couples ne sont plus choisis aléatoirement, mais au plus proche de la frontière de décision de l'algorithme (les paires où l'algorithme est le plus incertain). Puis, itérativement, l'algorithme est réentrainé sur les anciens et les nouveaux examples d'apprentissage fournis par l'opérateur. Au fil des itérations, les paires poussées à l'opérateur gagnent en pertinence ainsi que le modèle.
