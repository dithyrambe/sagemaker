MaBanque
========

.. toctree::
    :maxdepth: 2
    :caption: Construction de la matrice de similarité

    introduction.rst
    supervised_similarity
    active_learning
