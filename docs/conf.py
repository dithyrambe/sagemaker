import sys
import os

import sphinx_rtd_theme

def setup(app):
  app.add_stylesheet( "css/theme.css" )

extensions = ['sphinx.ext.autodoc', 'sphinx.ext.viewcode', 'sphinx.ext.napoleon',
              'sphinx.ext.todo']
napoleon_numpy_docstring = True

templates_path = ['_templates']

source_suffix = '.rst'

master_doc = 'index'

project = u'Documentation'
copyright = u"2018, DataLab"

version = "0.0.1"

release = version

exclude_patterns = ['_build']

pygments_style = 'sphinx'

html_theme = 'sphinx_rtd_theme'
html_title = "DataLab Documentation"
html_short_title = "DataLab Documentation"
html_logo = "logos/small_logo.jpg"
html_static_path = ['_static']

html_show_copyright = True
htmlhelp_basename = 'datalabdoc'
