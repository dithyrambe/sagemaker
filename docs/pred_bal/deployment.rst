Deploiement
===========


Bases DynamoDB
--------------
Deux bases de donnée DynamoDB ont été mises en place afin de contenir:
 - Les opérations récurrentes
 - Les soldes prédictifs (horizon fin de mois)


Batch journalier
----------------
Une fonction lambda est executée quotidiennement pour mettre à jour les soldes prédictifs de chacun des clients.
Elle permet d'identifier les nouvelles opérations réelles et de passer certaines opérations prédictives au statut "réalisée". Toutes les opérations récurrentes déjà réalisées sont à décompter du solde prédictif.


SNS
---
Les interractions avec la squad CloudNative se font exclusivement via le système de notification SNS.

Par défaut, toutes les opérations récurrentes identifiées par l'algorithme sont considérées comme valides.
A chaque nouvelle opération récurrente detectée, une notification est envoyée via SNS à l'application mobile afin que l'utilisateur valide ou invalide l'opération.
Si cette dernière est invalidée, elle ne sera plus comptabilisée dans le calcul du solde prédictif. 

Lorsqu'une opération est invalidée, le solde prédictif est mis à jour et une nouvelle notification est envoyée à l'application avec le dernier solde en vigueur.
