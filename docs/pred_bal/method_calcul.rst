Definition
==========

Les 3 composantes du solde predictif:
 - Le solde de début de mois
 - Les transactions réelles observées jusqu'à la date de calcul du solde
 - Les opérations récurrentes du mois qui n'ont pas encore été observées
