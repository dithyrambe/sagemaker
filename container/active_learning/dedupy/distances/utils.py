import string

import numpy as np

__all__ = [
    'StringConvolve',
]


class StringConvolve:
    VOCABULARY = string.ascii_uppercase + string.digits + ' '
    VOCAB_SIZE = len(VOCABULARY)

    @staticmethod
    def convert(s):
        return [StringConvolve.VOCABULARY.index(char) for char in s.upper()]

    @staticmethod
    def get_representations(str1, str2):
        repr1, repr2 = StringConvolve.convert(str1), StringConvolve.convert(str2)
        mat1, mat2 = np.zeros((len(repr1), StringConvolve.VOCAB_SIZE)), np.zeros(
            (len(repr2), StringConvolve.VOCAB_SIZE))
        mat1[tuple(zip(*enumerate(repr1)))] = 1
        mat2[tuple(zip(*enumerate(repr2)))] = 1
        return mat1, mat2

    @staticmethod
    def convolve(str1, str2):
        if str1 == '' or str2 == '':
            return np.asarray([0.0])
        repr1, repr2 = StringConvolve.get_representations(str1, str2)

        dot = repr2.dot(repr1.T)
        res = [np.trace(dot, offset=i) for i in range(-dot.shape[0] + 1, dot.shape[1])]
        return res
