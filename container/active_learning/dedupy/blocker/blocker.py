from abc import ABC, abstractmethod


class Blocker(ABC):

    @abstractmethod
    def fit(self, dataset):
        pass

    @abstractmethod
    def transform(self, dataset):
        pass

    def fit_transform(self, dataset):
        """Apply fit then transform given left & right"""
        self.fit(dataset)
        return self.transform(dataset)
