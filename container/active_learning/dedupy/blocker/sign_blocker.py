import numpy as np

from .blocker import Blocker


class SignBlocker(Blocker):

    def __init__(self, left_colname=None, right_colname=None):
        self.left_colname = left_colname
        self.right_colname = right_colname

    def fit(self, dataset):
        pass

    def transform(self, dataset):
        col_left_id = dataset.left_columns.index(self.left_colname)
        col_right_id = dataset.right_columns.index(self.right_colname)

        product = dataset.left[:, col_left_id][:, None] * dataset.right[:, col_right_id]
        to_compare = np.stack(np.where(product >= 0), axis=1)
        dataset.set_as_blocked(to_compare, np.zeros_like(to_compare))
        return dataset
