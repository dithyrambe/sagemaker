from six import with_metaclass

import abc

__all__ = [
    'QueryStrategy',
]


class QueryStrategy(with_metaclass(abc.ABCMeta)):
    """Pool-based query strategy
    A QueryStrategy advices on which unlabeled data to be queried next given
    a pool of labeled and unlabeled data.
    """

    def update(self, entry_id, label):
        """Update the internal states of the QueryStrategy after each queried
        sample being labeled.
        Parameters
        ----------
        entry_id : int
            The index of the newly labeled sample.
        label : float
            The label of the queried sample.
        """
        pass

    @abc.abstractmethod
    def make_query(self, dataset):
        """Return the index of the sample to be queried and labeled.
        parameters
        ----------
        dataset : dataset instance (must be set_as_ready for active learning)
        Returns
        -------
        ask_id : int
            The index of the next unlabeled sample to be queried and labeled.
        """
        pass
