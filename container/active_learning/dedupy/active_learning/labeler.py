from __future__ import print_function

from terminaltables import AsciiTable

__all__ = [
    'Labeler'
]

class Labeler:
    def __init__(self, dataset, features):
        """
        TODO: Here will be a Docstring one day ;)
        """
        self.dataset = dataset
        self.features = features

    def label(self, query_id):
        """
        repr, input, return
        :param query_id:
        :return:
        """
        indices = self.dataset.indices
        i_left, i_right = indices[query_id]

        unique_couples = self.get_unique_couples()

        left_cols, right_cols = zip(*unique_couples)

        left, right = self.dataset.left, self.dataset.right
        left_names, right_names = self.dataset.left_columns, self.dataset.right_columns
        sub_lnames = [left_names[l_col] for l_col in left_cols]
        sub_rnames = [right_names[r_col] for r_col in right_cols]

        table_data = [
            ['', 'LEFT', 'RIGHT'],
        ]

        for left_name, left_ind, right_name, right_ind in zip(sub_lnames, left_cols, sub_rnames, right_cols):
            table_data.append(
                ["{} | {}".format(
                    left_name, right_name
                ), "{}".format(
                    left[i_left, left_ind]
                ), "{}".format(
                    right[i_right, right_ind]
                )]
            )

        table = AsciiTable(table_data)
        print(table.table)
        ans = input("\n duplicates (y) or not (n):")
        while ans not in ['y', 'n', 'u', 'f']:
            ans = input("duplicates (y) or not (n):")
        if ans == 'y':
            return 1
        elif ans == 'n':
            return 0
        else:
            pass

    def get_unique_couples(self):
        translated_features = self.dataset.translate_parameters(self.features)
        left_cols = [f['left_on'] for f in translated_features]
        right_cols = [f['right_on'] for f in translated_features]

        couples = zip(left_cols, right_cols)
        unique_couples = []
        for couple in couples:
            if couple not in unique_couples:
                unique_couples.append(couple)
        return unique_couples
