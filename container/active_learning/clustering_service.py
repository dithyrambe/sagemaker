import os
import pickle
import numpy as np
import pandas as pd

from collections import Counter

from dedupy.dataset import Dataset
from dedupy.blocker.sign_blocker import SignBlocker
from dedupy.feature_engineering import FeatureCreator
from dedupy.active_learning import query_strategy as qs
from features_conf import FEATURING_WAY

prefix = '/opt/ml/'
model_path = os.path.join(prefix, 'model')


class ClusteringService:
    model = None

    @classmethod
    def _generate_transactions_ids_tuple(cls, dataset, ids_to_keep, inputs):
        ids1, ids2 = dataset.indices[ids_to_keep].T
        ids_trans1 = inputs["id"].values[ids1]
        ids_trans2 = inputs["id"].values[ids2]
        couples = np.array(["{},{}".format(id1, id2) for id1, id2 in zip(ids_trans1, ids_trans2)])
        return couples

    @classmethod
    def _get_clusters(cls, dataset, model, inputs):
        _, memberships = dataset.link(model, tol=0.7)
        value_counts = Counter(memberships)
        not_rec_ops = [kv[0] for kv in value_counts.items() if (kv[1] < 6) or (kv[1] > 30)]
        clusters = [-1 if x in not_rec_ops else x for x in memberships]
        temp = dict(zip(sorted(set(clusters))[1:], range(len(set(clusters)) - 1)))
        clusters = [temp.get(x, x) for x in clusters]
        inputs["cluster"] = clusters
        recurrent_op = inputs[inputs["cluster"] != -1]
        agg = recurrent_op.groupby("cluster")[["monthday", "s_amount", "label", "id"]].agg(
            {
                "monthday": "mean",
                "s_amount": "mean",
                "label": "last",
                "id": lambda x: list(set(x))
            }
        ).to_dict(orient="index")

        return agg

    @classmethod
    def _get_data_to_labelize(cls, query_strategy, model, dataset, inputs):
        to_labelize_id = np.array([query_strategy(model).make_query_valentin(dataset)])
        features = dataset._features[to_labelize_id]
        labels = dataset._labels[to_labelize_id]
        ids_to_keep = dataset._ids[to_labelize_id]

        trans_ids = np.array([cls._generate_transactions_ids_tuple(dataset, ids_to_keep, inputs)])

        labels = np.expand_dims(labels, axis=1)

        array = np.concatenate([trans_ids, features, labels], axis=1)
        db_al = pd.DataFrame(array).to_dict(orient="index")
        return db_al

    @classmethod
    def get_model(cls):
        if cls.model is None:
            with open(os.path.join(model_path, 'active_learning.pkl'), "rb") as active_learning_pkl:
                cls.model = pickle.load(active_learning_pkl)
        return cls.model

    @classmethod
    def predict(cls, inputs):
        dataset = Dataset(inputs)
        sign_blocker = SignBlocker(left_colname="s_amount", right_colname="s_amount")
        dataset = sign_blocker.fit_transform(dataset)
        feature_creator = FeatureCreator(FEATURING_WAY, n_jobs=4)
        dataset = feature_creator.fit_transform(dataset)
        query_strategy = qs.SklearnBoundaryStrategy
        model = cls.get_model()
        data_to_labelize = cls._get_data_to_labelize(query_strategy, model, dataset, inputs)
        recurrent_ops = cls._get_clusters(dataset, model, inputs)
        res = {
            "clusters": recurrent_ops,
            "data_to_labelize": data_to_labelize
        }
        return res
