import numpy as np

import dedupy.distances as strdist
from dedupy.distances import StringDistanceBase


class AmountDistance(StringDistanceBase):
    @staticmethod
    def _compute(str1, str2):
        """Squared error"""

        return (str1 - str2) ** 2

    @staticmethod
    def _norm_factor(str1, str2):
        """No relevant normalization factor"""
        return 1.


class TestAmountDistance(StringDistanceBase):
    @staticmethod
    def _compute(str1, str2):
        """Squared error"""

        return (str1 - str2) ** 2 / max((str1 + str2) ** 2, 0.1)

    @staticmethod
    def _norm_factor(str1, str2):
        """No relevant normalization factor"""
        return 1.


class ClosedPolarDayDist(StringDistanceBase):
    @staticmethod
    def _compute(str1, str2):
        """Days distance"""
        return (np.cos(str1) - np.cos(str2)) ** 2

    @staticmethod
    def _norm_factor(str1, str2):
        """No relevant normalization factor"""
        return 1.


class FakeDist(StringDistanceBase):
    @staticmethod
    def _compute(str1, str2):
        """Days distance"""
        return 0

    @staticmethod
    def _norm_factor(str1, str2):
        """No relevant normalization factor"""
        return 1.


FEATURING_WAY = [
    {
        'on': 'longLabel',
        'comparator': strdist.LevenshteinDistance()
    },
    {
        'on': 'label',
        'comparator': strdist.LevenshteinDistance()
    },
    {
        'on': 'label',
        'comparator': strdist.WordLevenshteinDistance()
    },
    {
        'on': 'label',
        'comparator': strdist.LongestCommonSubstring()
    },
    {
        'on': 'date',
        'comparator': FakeDist()
    },
    {
        'on': 'closed_polar_day',
        'comparator': ClosedPolarDayDist()
    },
    {
        'on': 's_amount',
        'comparator': TestAmountDistance()
    }
]